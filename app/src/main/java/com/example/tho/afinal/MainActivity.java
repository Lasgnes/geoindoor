package com.example.tho.afinal;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.lize.oledcomm.camera_lifisdk_android.ILiFiPosition;
import com.lize.oledcomm.camera_lifisdk_android.LiFiSdkManager;
import com.lize.oledcomm.camera_lifisdk_android.V1.LiFiCamera;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, LocationListener {

    private EditText number;
    private Button sender;
    private Spinner contactList;
    private static MainActivity activity;

    private double latitude;
    private double longitude;

    private LiFiSdkManager liFiSdkManager;
    private String idLiFi;

    private HashMap<String, String> coordLiFi;
    private TreeMap<String, String> contacts;

    public static MainActivity instance() {
        return activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainactivity);

        ActivityCompat.requestPermissions(MainActivity.this, new String[]{"android.permission.CAMERA"}, 47);
        coordLiFi = new HashMap<>();
        coordLiFi.put("00111100", "39.03385,125.75432");
        coordLiFi.put("11000011", "60.192059,24.945831");
        coordLiFi.put("10101010", "59.93863,30.31413");

        liFiSdkManager = new LiFiSdkManager(this, LiFiSdkManager.CAMERA_LIB_VERSION_0_1,
                "token", "user", new ILiFiPosition() {
            @Override
            public void onLiFiPositionUpdate(String lamp) {
                idLiFi = lamp;
            }
        });

        liFiSdkManager.setLocationRequestMode(LiFiSdkManager.LOCATION_REQUEST_OFFLINE_MODE);
        liFiSdkManager.init(R.id.main, LiFiCamera.FRONT_CAMERA);
        liFiSdkManager.start();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.READ_CONTACTS"}, 47);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.READ_SMS"}, 501);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.ACCESS_FINE_LOCATION"}, 2);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.ACCESS_COARSE_LOCATION"}, 3);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        activity = this;
        number = findViewById(R.id.number);
        sender = findViewById(R.id.sender);
        contactList = findViewById(R.id.contacts);

        getContactList();
        for(String key: contacts.keySet()) {
            Log.d("contacts", key + " " + contacts.get(key));
        }

        sender.setOnClickListener(this);

        ArrayAdapter<String> adapter =new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, new ArrayList<>(contacts.keySet()));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        contactList.setAdapter(adapter);
        contactList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String contact = String.valueOf(contactList.getSelectedItem());
                Log.d("contacts", "contact selected: " + contact);
                number.setText(contacts.get(contact));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (liFiSdkManager != null && liFiSdkManager.isStarted()) {
            liFiSdkManager.stop();
            liFiSdkManager.release();
            liFiSdkManager = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (liFiSdkManager != null && liFiSdkManager.isStarted()) {
            liFiSdkManager.stop();
            liFiSdkManager.release();
            liFiSdkManager = null;
        }
    }

    @Override
    public void onClick(View v) {
        if (v.equals(sender)) {
            String value = number.getText().toString();
            if (value.length() == 10)
                sendSMSMessage(value, getString(R.string.where));
            else {
                Toast.makeText(this, "Not a valid phone number", Toast.LENGTH_LONG).show();
            }
        }
    }

    //send a SMS
    protected void sendSMSMessage(String numero, String message) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(numero, null, message, null, null);
    }

    //start a new map activity
    public void goToMap(String coords) {
        if (liFiSdkManager != null && liFiSdkManager.isStarted()) {
            liFiSdkManager.stop();
            liFiSdkManager.release();
            liFiSdkManager = null;
        }
        Log.d("coord", coords);

        //get location of the searching phone
        Location location = getCoord();
        String myCoords;
        if (location != null) {
            myCoords = String.valueOf(location.getLatitude()) + " " + String.valueOf(location.getLongitude());
        } else {
            myCoords = "0 0";
        }

        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra("coords", coords);
        intent.putExtra("myCoords", myCoords);
        Log.d("activity", "Starting mapactivity");
        startActivity(intent);
    }

    //get the last known location
    public Location getCoord() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }
        Log.d("coord", "gettin'");

        //iterate through all providergit
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null
                    || l.getAccuracy() < bestLocation.getAccuracy()) {
                Log.d("coord", String.valueOf(l.getLatitude()) + " " + String.valueOf(l.getLongitude()));
                bestLocation = l;
            }
        }
        if (bestLocation == null) {
            return null;
        }
        return bestLocation;
    }

    //transform the gps coordinates into a string
    public String coord() {
        Log.d("coord", "tryin'");
        return String.valueOf(latitude) + " " + String.valueOf(longitude);
    }

    //Lifi coords
    private void getCoordFromLiFi() {
        try {
            String[] tmp = idLiFi.split(",");
            latitude = Double.valueOf(tmp[0]);
            longitude = Double.valueOf(tmp[1]);
            Log.d("coord", "LiFi: " + latitude + longitude);
        } catch (Exception e) {
            //default
            latitude = 52.9408299;
            longitude = -1.1743992;
            Log.d("coord", "Go to Forge World: " + latitude + longitude);
        }
    }

    //updates the gps coordinates
    public void updateGPSCoordinates(Location location){
        Log.d("coord", "updatin'");
        if (location != null && (location.getLatitude() != 0 && location.getLongitude() != 0)) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            Log.d("coord", "New coords: " + latitude + longitude);
        } else {
            getCoordFromLiFi();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        updateGPSCoordinates(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    //retreive phone's contacts list
    private void getContactList() {
        contacts = new TreeMap();
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));

                if (cur.getInt(cur.getColumnIndex(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));
                        Log.d("contact", "Name: " + name);
                        Log.d("contact", "Phone Number: " + phoneNo);
                        phoneNo = rewriteNo(phoneNo);
                        if (phoneNo.length() == 10) {
                            contacts.put(name + ": " + phoneNo, phoneNo);
                        }
                    }
                    pCur.close();
                }
            }
        }
        if(cur!=null){
            cur.close();
        }
    }

    private String rewriteNo(String phoneNo){
        phoneNo = phoneNo.replaceAll(" ", "");
        phoneNo = phoneNo.replaceAll("\\+\\d\\d", "0");
        return phoneNo;
    }


}
