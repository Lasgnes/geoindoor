package com.example.tho.afinal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Théo on 15/11/2017.
 */

public class SMSReceiver extends BroadcastReceiver {

    final SmsManager sms = SmsManager.getDefault();

    public void onReceive(Context context, Intent intent) {

        // Get the SMS message received
        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                // A PDU is a "protocol data unit". This is the industrial standard for SMS message
                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (int i = 0; i < pdusObj.length; i++) {
                    SmsMessage sms = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = sms.getDisplayOriginatingAddress();
                    String message = sms.getDisplayMessageBody();
                    String formattedText = "From " + phoneNumber + ": " + message;
                    Toast.makeText(context, formattedText, Toast.LENGTH_LONG).show();
                    MainActivity inst = MainActivity.instance();
                    vibrate(inst);
                    Log.d("msg", "received message: " + message);
                    if (message.matches("-*\\d*\\.*\\d* -*\\d*\\.*\\d*")) {
                        Log.d("msg", "received coords: " + message);
                        inst.goToMap(message);
                    } else if (message.contains(inst.getString(R.string.where))) {
                        Log.d("msg", "received request: " + message);
                        inst.updateGPSCoordinates(inst.getCoord());
                        sendSMSMessage(phoneNumber, inst.coord());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void sendSMSMessage(String numero, String message) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(numero, null, message, null, null);
    }

    protected void vibrate(MainActivity inst){
        Vibrator vibe = (Vibrator) inst.getSystemService(Context.VIBRATOR_SERVICE);
        vibe.vibrate(1000);
    }
}
